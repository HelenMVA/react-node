import express from "express";
import UserController from "./controller";

const router = express.Router();

router.get("/:id", UserController.getUser);
router.post("/register", UserController.register);
router.post("/authenticate", UserController.authenticate);
router.get("/", UserController.getUsers);

export default router;
