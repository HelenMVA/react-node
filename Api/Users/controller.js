import UserServices from "./service";

const UserController = {
  getUser: (req, res) => {
    UserServices.getUser(req, result => {
      result.success ? res.status(200).send(result) : res.status(404).send(result)
    })
  },
  authenticate: (req, res) => {
    UserServices.authenticate(req.body).then(result =>
      res.status(result.status).send(result.payload)
    );
  },
  register: async (req, res) => {
    UserServices.register(req.body).then(result =>
      res.status(result.status).send(result.payload)
    );
  },
  getUsers: (req, res) => {
    UserServices.getUsers(req, result => {
      result.success ? res.status(200).send(result) : res.status(404).send(result)
    })
  },
};

export default UserController;
