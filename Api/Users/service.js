import UserQueries from "./query";
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";
import config from "../config/server.json";
import Queries from "./query";

const UserServices = {
  getUser: (req, callback) => {
    UserQueries.getUser(
      req,
      (response) => {
        return callback({ success: true, message: response });
      },
      (error) => {
        return callback({ success: false, message: error });
      }
    );
  },


  getUsers: (req, callback) => {
    UserQueries.getUsers(
      req,
      (response) => {
        return callback({ success: true, message: response });
      },
      (error) => {
        return callback({ success: false, message: error });
      }
    );
  },


  authenticate: async (body) => {
    let { email, password } = body;

    if (typeof email !== "string" || typeof password !== "string") {
      return {
        status: 400,
        payload: {
          success: false,
          message: "All fields are required and must be a string type",
        },
      };
    }

    const user = await UserQueries.getByUserEmail(email);

    if (!user) {
      return {
        status: 403,
        payload: { success: false, message: "Username not found" },
      };
    }

    const passwordMatched = await bcrypt.compare(password, user.password);

    if (passwordMatched) {
      const token = jwt.sign(
        { id: user.id, role: user.user_role },
        config.secret
      );

      const { password, ...userWithoutPassword } = user;
      return {
        status: 200,
        payload: {
          success: true,
          message: "User correctly authenticated",
          data: { token: token, user: userWithoutPassword },
        },
      };
    }

    return {
      status: 403,
      payload: { success: false, message: "Username & password missmatch" },
    };
  },
  register: async (body) => {
    let { username, email, password } = body;
    if (
      typeof username !== "string" ||
      typeof email !== "string" ||
      typeof password !== "string"
    ) {
      return {
        status: 400,
        payload: {
          success: false,
          message: "All fields are required and must be a string type",
        },
      };
    }
    const user = await Queries.getByUserEmail(email);

    if (user) {
      return {
        status: 403,
        payload: { success: false, message: "Username existe" },
      };
    }

    return bcrypt
      .genSalt()
      .then((salt) => bcrypt.hash(password, salt))
      .then((hashedPassword) =>
        Queries.register({ username, email, hashedPassword })
      )
      .then((user) => ({
        status: 201,
        payload: { success: true, message: "User successfully registered" },
      }))
      .catch((err) => ({
        status: 400,
        payload: { success: false, message: err },
      }));
  },
};

export default UserServices;
