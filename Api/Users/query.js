import db, { database } from "../setup/database";

const Queries = {
	authenticate: (user, successCallback, failureCallback) => {
		let sqlQuery = `SELECT * FROM users WHERE name="${user.email}" AND password="${user.password}"`;

		db.query(sqlQuery, (err, rows) => {
			if (err) {
				return failureCallback(err);
			}
			if (rows.length > 0) {
				return successCallback(rows[0]);
			} else {
				return successCallback("Incorrect username or password combinaison");
			}
		});
	},
	register: async (users) => {
		return new Promise((resolve, reject) => {
			let sqlQuery = `INSERT INTO users (id, username, email, password, role) VALUES (NULL,"${users.username}", "${users.email}", "${users.hashedPassword}", "USER")`;

			db.query(sqlQuery, (err, res) => {
				if (err) reject(err);
				resolve(res);
			});
		});
	},
	getByUserEmail: (email) => {
		let sqlQuery = `SELECT * FROM users WHERE email="${email}"`;

		return new Promise((resolve, reject) => {
			db.query(sqlQuery, (err, rows) => {
				if (err) reject(err);
				resolve(rows[0]);
			});
		});
	},
	getByUsername: (username) => {
		let sqlQuery = `SELECT * FROM users WHERE username="${username}"`;

		return new Promise((resolve, reject) => {
			db.query(sqlQuery, (err, rows) => {
				if (err) reject(err);
				resolve(rows[0]);
			});
		});
	},
	getUser: (param, successCallback, failureCallback) => {
		let sqlQuery = `SELECT user_has_book.date_borrowed, user_has_book.date_return, user_has_book.returned,
                          users.username, users.email, books.book_title, books.id, books.img, author.firstname, author.lastname
                    FROM user_has_book, users, books, author
                    WHERE user_has_book.id_user = users.id AND user_has_book.id_livre = books.id AND books.auter_id = author.id
                    AND users.id = ${param.params.id}`;

		db.query(sqlQuery, (err, rows) => {
			if (err) {
				return failureCallback(err);
			}
			if (rows.length > 0) {
				return successCallback(rows);
			} else {
				return successCallback("No matching user");
			}
		});
	},

	getUsers: (param, successCallback, failureCallback) => {
		let sqlQuery = `SELECT user_has_book.date_borrowed, user_has_book.date_return, user_has_book.returned,
                          users.username, users.email, books.book_title, books.id, books.img, author.firstname, author.lastname
                    FROM user_has_book, users, books, author
                    WHERE user_has_book.id_user = users.id AND user_has_book.id_livre = books.id AND books.auter_id = author.id AND returned=false`;

		db.query(sqlQuery, (err, rows) => {
			if (err) {
				return failureCallback(err);
			}
			if (rows.length > 0) {
				return successCallback(rows);
			} else {
				return successCallback("No matching user");
			}
		});
	},
};

export default Queries;
