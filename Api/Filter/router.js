import express from "express";
import FilterController from "./controller";

const router = express.Router();

router.get("/author/:id", FilterController.getBookByAuthor);
router.get("/category/:id", FilterController.getBookByCategory);
router.post("/search", FilterController.searchBook);

export default router;
