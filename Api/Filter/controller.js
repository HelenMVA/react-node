import FilterServices from "./service";

const FilterController = {
    getBookByCategory: (req, res) => {
        FilterServices.getBookByCategory(req, result => {
            result.success ? res.status(200).send(result) : res.status(404).send(result)
        })
    },
    getBookByAuthor: (req, res) => {
        FilterServices.getBookByAuthor(req, result => {
            result.success ? res.status(200).send(result) : res.status(404).send(result)
        })
    },
    searchBook: (req, res) => {
        console.log(req.body)
        FilterServices.searchBook(req)
            .then(result => res.status(result.status).send(result.payload))
    },
};

export default FilterController;
