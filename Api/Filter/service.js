import Queries from "./query";

const FilterServices = {
  getBookByCategory: (req, callback) => {
    Queries.getBookByCategory(
      req,
      (response) => {
        return callback({
          success: true,
          message: "Books by category retrieve",
          data: response,
        });
      },
      (error) => {
        return callback({ success: false, message: error });
      }
    );
  },
  getBookByAuthor: (req, callback) => {
    Queries.getBookByAuthor(
      req,
      (response) => {
        return callback({
          success: true,
          message: "Book by author retrieve",
          data: response,
        });
      },
      (error) => {
        return callback({ success: false, message: error });
      }
    );
    },
    searchBook: (req) => {
      Queries.searchBook(
        req,
        (response) => {
          return callback({
            success: true,
            message: "Book by author retrieve",
            data: response,
          });
        },
        (error) => {
          return callback({ success: false, message: error });
        }
      );
    }
};

export default FilterServices;