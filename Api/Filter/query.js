import db, { database } from "../setup/database";

const Queries = {
  getBookByCategory: (param, successCallback, failureCallback) => {
    let sqlQuery = "SELECT * FROM `books` WHERE category_id =" + param.params.id;

    db.query(sqlQuery, (err, rows) => {
      if (err) {
        return failureCallback(err);
      }
      if (rows.length > 0) {
        return successCallback(rows);
      } else {
        return successCallback("No category for this book");
      }
    });
  },
  getBookByAuthor: (param, successCallback, failureCallback) => {
    let sqlQuery = "SELECT * FROM `books` WHERE auter_id =" + param.params.id;

    db.query(sqlQuery, (err, rows) => {
      if (err) {
        return failureCallback(err);
      }
      if (rows.length > 0) {
        return successCallback(rows);
      } else {
        return successCallback("No book with this author");
      }
    });
  },
  searchBook: (param) => {
    let sqlQuery = "SELECT * FROM `books` where title LIKE '% "+ 
    param.body.title + "%'"

    db.query(sqlQuery, (err, rows) => {
      if (err) {
        return err;
      }
      if (rows.length > 0) {
        return rows;
      }
    });
  },
}

export default Queries;
