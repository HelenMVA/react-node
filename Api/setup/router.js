import express from "express";
import usersRoutes from "../Users/router";
import homeRoutes from "../Home/router";
import bookRoutes from "../Books/router";
import filterRoutes from "../Filter/router"
import authorRoutes from "../Authors/router"
import adminRoutes from "../Admin/router"

const Router = (server) => {
  server.use("/api/user", usersRoutes);
  server.use("/api/home", homeRoutes);
  server.use("/api/book", bookRoutes);
  server.use("/api/filter", filterRoutes);
  server.use("/api/author", authorRoutes);
  server.use("/api/admin", adminRoutes);
};

export default Router;
