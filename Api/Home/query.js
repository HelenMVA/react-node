import db, { database } from "../setup/database";

const Queries = {
  getHome: (param, successCallback, failureCallback) => {
    let sqlQuery = "SELECT * FROM `home`";

    db.query(sqlQuery, (err, rows) => {
      if (err) {
        return failureCallback(err);
      }
      if (rows.length > 0) {
        return successCallback(rows);
      } else {
        return successCallback("No data in homepage.");
      }
    });
  },
  getFeatures: (param, successCallback, failureCallback) => {
    let sqlQuery = "SELECT * FROM `functionality`";

    db.query(sqlQuery, (err, rows) => {
      if (err) {
        return failureCallback(err);
      }
      if (rows.length > 0) {
        return successCallback(rows);
      } else {
        return successCallback("No data in features.");
      }
    });
  },
};

export default Queries;
