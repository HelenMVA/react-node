import Queries from "./query";

const HomeServices = {
  getHomePage: (req, callback) => {
    Queries.getHome(
      req,
      (response) => {
        return callback({
          success: true,
          message: "HomePage retrieve",
          data: response,
        });
      },
      (error) => {
        return callback({ success: false, message: error });
      }
    );
  },
  getFeatures: (req, callback) => {
    Queries.getFeatures(
      req,
      (response) => {
        return callback({
          success: true,
          message: "Features retrieve",
          data: response,
        });
      },
      (error) => {
        return callback({ success: false, message: error });
      }
    );
  },
};

export default HomeServices;
