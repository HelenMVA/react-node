import HomeServices from "./service";

const HomeController = {
    getHomepage: (req, res) => {
        HomeServices.getHomePage(req, result => {
            result.success ? res.status(200).send(result) : res.status(404).send(result)
        })
    },
    getFeatures: (req, res) => {
        HomeServices.getFeatures(req, result => {
            result.success ? res.status(200).send(result) : res.status(404).send(result)
        })
    },
};

export default HomeController;
