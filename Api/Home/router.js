import express from "express";
import HomeController from "./controller";

const router = express.Router();

router.get("/", HomeController.getHomepage);
router.get("/features", HomeController.getFeatures);

export default router;
