import db, { database } from "../setup/database";

const Queries = {
  getBooks: (param, successCallback, failureCallback) => {
    let sqlQuery = "SELECT books.book_title, books.id, books.dispo, books.img, books.auter_id, books.category_id, books.resume, books.date, books.slug, author.firstname, author.lastname FROM books, author WHERE books.auter_id = author.id";

    db.query(sqlQuery, (err, rows) => {
      if (err) {
        return failureCallback(err);
      }
      if (rows.length > 0) {
        return successCallback(rows);
      } else {
        return successCallback("No data in books/authors.");
      }
    });
  },
  getBook: (param, successCallback, failureCallback) => {
    console.log(param.params.slug)
    // let sqlQuery = "SELECT * FROM `books`, author WHERE books.auter_id = author.id AND books.slug=" + "${param.body.id_user}";
    let sqlQuery = `SELECT books.id, books.book_title, books.img, books.auter_id, books.resume, books.dispo, author.firstname, author.lastname, author.biography FROM books, author WHERE books.auter_id = author.id AND books.slug="${param.params.slug}"`
    console.log(sqlQuery)

    db.query(sqlQuery, (err, rows) => {
      if (err) {
        return failureCallback(err);
      }
      if (rows.length > 0) {
        return successCallback(rows);
      } else {
        return successCallback("No book with this id");
      }
    });
  },
  getCategories: (param, successCallback, failureCallback) => {
    let sqlQuery = "SELECT * FROM `category`";

    db.query(sqlQuery, (err, rows) => {
      if (err) {
        return failureCallback(err);
      }
      if (rows.length > 0) {
        return successCallback(rows);
      } else {
        return successCallback("No data in categories.");
      }
    });
  },
  borrowBook: (param, successCallback, failureCallback) => {
    let sqlQuery = `INSERT INTO user_has_book (id, id_user, id_livre, date_borrowed, date_return, returned) VALUES (NULL,"${param.body.id_user}", "${param.body.id_book}", "${param.body.date_borrowed}", "${param.body.date_return}", 0)`;

      db.query(sqlQuery, (err) => {
        if (err) {
          return failureCallback(err);
        } else {
          return successCallback("livre emprunter")
        }
      });
  },
  bookAvailable: (param, successCallback, failureCallback) => {
    let sqlQuery = `UPDATE books SET dispo="false" WHERE id=${param.body.id}`;

      db.query(sqlQuery, (err) => {
        if (err) {
          return failureCallback(err);
        } else {
          return successCallback("livre non dispo")
        }
      });
  },
  bookAvailableTrue: (param, successCallback, failureCallback) => {
    let sqlQuery = `UPDATE books SET dispo="true" WHERE id=${param.body.id}`;

      db.query(sqlQuery, (err) => {
        if (err) {
          return failureCallback(err);
        } else {
          return successCallback("livre dispo")
        }
      });
  },
  returnBook: (param, successCallback, failureCallback) => {
    let sqlQuery = `UPDATE user_has_book SET returned = true WHERE id_livre=${param.body.id}`;

      db.query(sqlQuery, (err) => {
        if (err) {
          return failureCallback(err);
        } else {
          return successCallback("livre rendu")
        }
      });
  },
}

export default Queries;
