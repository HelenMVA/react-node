import BookServices from "./service";

const BookController = {
    getBooks: (req, res) => {
        BookServices.getBooks(req, result => {
            result.success ? res.status(200).send(result) : res.status(404).send(result)
        })
    },
    getBook: (req, res) => {
        BookServices.getBook(req, result => {
            result.success ? res.status(200).send(result) : res.status(404).send(result)
        })
    },
    borrowBook: (req, res) => {
        BookServices.borrowBook(req, result => {
            console.log(result)
                result.success
                    ? res.status(201).send(result)
                    : res.status(404).send(result)
        })
      },
      getCategories: (req, res) => {
        BookServices.getCategories(req, result => {
            result.success ? res.status(200).send(result) : res.status(404).send(result)
        })
    },
    bookAvailable: (req, res) => {
        BookServices.bookAvailable(req, result => {
            console.log(result)
                result.success
                    ? res.status(201).send(result)
                    : res.status(404).send(result)
        })
      },
      bookAvailableTrue: (req, res) => {
        BookServices.bookAvailableTrue(req, result => {
            console.log(result)
                result.success
                    ? res.status(201).send(result)
                    : res.status(404).send(result)
        })
      },
      returnBook: (req, res) => {
        BookServices.returnBook(req, result => {
            console.log(result)
                result.success
                    ? res.status(201).send(result)
                    : res.status(404).send(result)
        })
      },
};

export default BookController;
