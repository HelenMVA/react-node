import Queries from "./query";

const BookServices = {
  getBooks: (req, callback) => {
    Queries.getBooks(
      req,
      (response) => {
        return callback({
          success: true,
          message: "Books and authors retrieve",
          data: response,
        });
      },
      (error) => {
        return callback({ success: false, message: error });
      }
    );
  },
  getBook: (req, callback) => {
    Queries.getBook(
      req,
      (response) => {
        return callback({
          success: true,
          message: "Book retrieve",
          data: response,
        });
      },
      (error) => {
        return callback({ success: false, message: error });
      }
    );
  },
  getCategories: (req, callback) => {
    Queries.getCategories(
      req,
      (response) => {
        return callback({
          success: true,
          message: "Category retrieve",
          data: response,
        });
      },
      (error) => {
        return callback({ success: false, message: error });
      }
    );
  },
  borrowBook: (req, callback) => {
    Queries.borrowBook(
      req,
      response => {
        return callback({ success: true, message: response });
      },
      error => {
        return callback({ success: false, message: error });
      }
    )
  },
  bookAvailable: (req, callback) => {
    Queries.bookAvailable(
      req,
      response => {
        return callback({ success: true, message: response });
      },
      error => {
        return callback({ success: false, message: error });
      }
    )
  },
  bookAvailableTrue: (req, callback) => {
    Queries.bookAvailableTrue(
      req,
      response => {
        return callback({ success: true, message: response });
      },
      error => {
        return callback({ success: false, message: error });
      }
    )
  },
  returnBook: (req, callback) => {
    Queries.returnBook(
      req,
      response => {
        return callback({ success: true, message: response });
      },
      error => {
        return callback({ success: false, message: error });
      }
    )
  },
};

export default BookServices;
