import express from "express";
import BookController from "./controller";

const router = express.Router();

router.get("/", BookController.getBooks);
router.get("/categories", BookController.getCategories);
router.get("/:slug", BookController.getBook);
router.post("/borrow", BookController.borrowBook);
router.put("/available", BookController.bookAvailable)
router.put("/available/true", BookController.bookAvailableTrue)
router.put("/return", BookController.returnBook)


export default router;
