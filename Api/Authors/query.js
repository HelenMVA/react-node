import db, { database } from "../setup/database";

const Queries = {
    getAuthors: (param, successCallback, failureCallback) => {

        let sqlQuery = "SELECT * FROM `author`";

        db.query(sqlQuery, (err, rows) => {
            if (err) {
                return failureCallback(err);
            }
            if (rows.length > 0) {
                return successCallback(rows);
            } else {
                return successCallback("No data for author.");
            }
        })
    },
}
   
export default Queries;
