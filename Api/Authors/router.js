import express from "express";
import AuthorController from "./controller";

const router = express.Router();

router.get("/", AuthorController.getAuthors);
 
export default router;
