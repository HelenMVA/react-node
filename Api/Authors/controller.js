import AuthorServices from "./service";

const AuthorController = {
    getAuthors: (req, res) => {
        AuthorServices.getAuthors(req, result => {
            result.success ? res.status(200).send(result) : res.status(404).send(result)
        })
    },
};

export default AuthorController;
