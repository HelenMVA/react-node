import Queries from "./query";

const AuthorServices = {
    getAuthors: (req, callback) => {
        Queries.getAuthors(req,
            response => {
                return callback({ success: true, message: 'Authors retrieve', data: response });
            },
            error => {
                return callback({ success: false, message: error });
            });
    }
};

export default AuthorServices;
