import db, { database } from "../setup/database";

const Queries = {
	registerBook: (param, successCallback, failureCallback) => {
		let sqlQuery = `INSERT INTO books (id, book_title, img, auter_id, resume, date, dispo, category_id, slug) VALUES (NULL,"${param.body.book_title}", "${param.body.img}","${param.body.auter_id}", "${param.body.resume}", "${param.body.date}", "true", "${param.body.category_id}", "${param.body.slug}")`;
		db.query(sqlQuery, (err) => {
			if (err) {
				return failureCallback(err);
			} else {
				return successCallback("livre post");
			}
		});
	},
	deleteBook: (param, successCallback, failureCallback) => {
		let sqlQuery = `DELETE FROM books WHERE id=${param.params.id}`;

		db.query(sqlQuery, (err) => {
			if (err) {
				return failureCallback(err);
			} else {
				return successCallback("livre supprimer");
			}
		});
	},
};

export default Queries;
