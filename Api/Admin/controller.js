import AdminServices from "./service";

const AdminController = {
	registerBook: (req, res) => {
		AdminServices.registerBook(req, (result) => {
			console.log(result);
			result.success
				? res.status(201).send(result)
				: res.status(404).send(result);
		});
	},

	deleteBook: (req, res) => {
		AdminServices.deleteBook(req, (result) => {
			console.log(result);
			result.success
				? res.status(201).send(result)
				: res.status(404).send(result);
		});
	},
};

export default AdminController;
