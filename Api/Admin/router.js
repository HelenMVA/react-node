import express from "express";
import AdminController from "./controller";

const router = express.Router();

router.post("/registerBook", AdminController.registerBook);
router.delete("/book/:id", AdminController.deleteBook);

export default router;
