import Queries from "./query";

const AdminServices = {
	registerBook: (req, callback) => {
		Queries.registerBook(
			req,
			(response) => {
				return callback({ success: true, message: response });
			},
			(error) => {
				return callback({ success: false, message: error });
			}
		);
	},
	deleteBook: (req, callback) => {
		Queries.deleteBook(
			req,
			(response) => {
				return callback({ success: true, message: response });
			},
			(error) => {
				return callback({ success: false, message: error });
			}
		);
	},
};

export default AdminServices;
