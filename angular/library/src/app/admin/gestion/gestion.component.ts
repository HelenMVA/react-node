import { Component, OnInit } from "@angular/core";
import { BookService } from "src/app/services/book.service";
import { Router } from "@angular/router";
import { AdminService } from "src/app/services/admin.service";
import { UserService } from "src/app/services/user.service";

@Component({
  selector: "app-gestion",
  templateUrl: "./gestion.component.html",
  styleUrls: ["./gestion.component.scss"],
})
export class GestionComponent implements OnInit {
  allBooks: any;
  borrowBooks: any;
  toBeReturned: any;

  currentUserId: any;
  myCurrentsBooks: [] = [];

  constructor(
    private booksService: BookService,
    private adminService: AdminService,
    private userService: UserService,
    private router: Router
  ) {}

  ngOnInit() {
    this.getBooks();
    this.getUsers();
  }

  getUsers() {
    this.adminService.getUsers().subscribe((res: any) => {
      this.toBeReturned = res.message;
      console.log(this.toBeReturned.length);
    });
  }

  getBooks() {
    this.booksService.getAllBooks().subscribe((res: any) => {
      this.allBooks = res.data;
      console.dir(this.allBooks);
    });
  }
}
