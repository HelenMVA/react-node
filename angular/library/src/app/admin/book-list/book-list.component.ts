import { Component, OnInit } from "@angular/core";
import { BookService } from "src/app/services/book.service";
import { Router } from "@angular/router";
import { AdminService } from "src/app/services/admin.service";

@Component({
  selector: "app-book-list",
  templateUrl: "./book-list.component.html",
  styleUrls: ["./book-list.component.scss"],
})
export class BookListComponent implements OnInit {
  allBooks: any;
  constructor(
    private booksService: BookService,
    private adminService: AdminService,
    private router: Router
  ) {}

  ngOnInit() {
    this.getBooks();
  }
  getBooks() {
    this.booksService.getAllBooks().subscribe((res: any) => {
      this.allBooks = res.data;
      console.dir(this.allBooks);
    });
  }
  goByBookId(id) {
    console.dir(id);
    this.router.navigate(["books/" + id], { state: { id } });
  }

  editBook(id) {
    console.dir(id);
    this.router.navigate(["editBook/" + id], { state: { id } });
  }
  deleteBook(id) {
    console.log(id);
    this.adminService.deleteBook(id).subscribe((res: any) => {
      console.log(res);
      this.getBooks();
    });
  }
}
