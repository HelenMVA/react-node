import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModelAddBookComponent } from './model-add-book.component';

describe('ModelAddBookComponent', () => {
  let component: ModelAddBookComponent;
  let fixture: ComponentFixture<ModelAddBookComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModelAddBookComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModelAddBookComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
