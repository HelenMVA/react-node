import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { BookService } from "src/app/services/book.service";
import { Router } from "@angular/router";
import { AuthorServices } from "../../services/author.service";
import { AdminService } from "../../services/admin.service";

@Component({
  selector: "app-model-add-book",
  templateUrl: "./model-add-book.component.html",
  styleUrls: ["./model-add-book.component.scss"],
})
export class ModelAddBookComponent implements OnInit {
  FormAddBook: FormGroup = this.fb.group({
    book_title: ["", Validators.compose([Validators.required])],
    img: ["", Validators.compose([Validators.required])],
    auter_id: ["", Validators.compose([Validators.required])],
    resume: ["", Validators.compose([Validators.required])],
    date: ["", Validators.compose([Validators.required])],
    category_id: ["", Validators.compose([Validators.required])],
    slug: ["", Validators.compose([Validators.required])],
  });

  allBooks: any;
  allCategory: [];
  allAuthors: [];
  constructor(
    public activeModal: NgbActiveModal,
    private fb: FormBuilder,
    private booksService: BookService,
    private authorService: AuthorServices,
    private adminService: AdminService
  ) {}

  ngOnInit() {
    this.getAuthors();
    this.getAllCategory();
  }

  addBook() {
    console.dir(this.FormAddBook.value);
    this.adminService.registerBook(this.FormAddBook.value).subscribe(
      (data) => {
        console.dir(data);
        this.closeModal();
      },
      (err) => {
        console.dir(err);
      }
    );
  }

  closeModal() {
    this.activeModal.close();
  }
  getAllCategory() {
    this.adminService.getAllCategories().subscribe((res: any) => {
      this.allCategory = res.data;
      console.dir(this.allCategory);
    });
  }
  getAuthors() {
    this.authorService.getAuthors().subscribe((res: any) => {
      this.allAuthors = res.data;
      console.dir(this.allAuthors);
    });
  }

  // getAllCategory() {
  //   this.booksService.getAllCategory().subscribe((res: any) => {
  //     this.allCategory = res.data;
  //     console.dir(this.allCategory);
  //   });
  // }
  



}
