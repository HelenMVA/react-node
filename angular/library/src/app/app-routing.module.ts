import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { HomePageComponent } from "./pages/home-page/home-page.component";
import { BooksPageComponent } from "./pages/books-page/books-page.component";
import { BookDetailsPageComponent } from "./pages/book-details-page/book-details-page.component";
import { DashboardComponent } from "./pages/dashboard/dashboard.component";
import { HomeDashboardComponent } from "./components/home-dashboard/home-dashboard.component";
import { HistoryComponent } from "./components/history/history.component";
import { ProfileComponent } from "./components/profile/profile.component";
import { BorrowedBooksComponent } from "./components/borrowed-books/borrowed-books.component";
import { ModelAddBookComponent } from './admin/model-add-book/model-add-book.component';
import { AdminDachboardPageComponent } from './pages/admin-dachboard-page/admin-dachboard-page.component';
import { BookListComponent } from './admin/book-list/book-list.component';
import { UserService } from './services/user.service';
import { AdminService } from './services/admin.service';
import { GestionComponent } from './admin/gestion/gestion.component';



const routes: Routes = [
  // Routes public

  {
    path: '',
    redirectTo: '/home',
    pathMatch: 'full'

  },

  {
    path: 'home',
    component: HomePageComponent
  },

  {
    path: "books",
    component: BooksPageComponent,
    children: [{ path: ":slug", component: BookDetailsPageComponent }],
  },


  // Routes admin
  {
    path: "home/admin",
    component: AdminDachboardPageComponent,
    children: [
      { path: "addbook", component: ModelAddBookComponent},
      { path: "booklist", component: BookListComponent },
      { path: "gestion", component: GestionComponent },
    ]
  },


  // Logged Routes
  {
    path: "dashboard",
    component: DashboardComponent,
    canActivate: [UserService],
    children: [
      {
        path: "browse-book",
        component: BooksPageComponent,
        canActivate: [UserService],
        children: [{ path: "book/:slug", component: BookDetailsPageComponent, canActivate: [UserService] }],
      },
      {
        path: "admin",
        component: AdminDachboardPageComponent,
      },
      {
        path: "home",
        component: HomeDashboardComponent,
        canActivate: [UserService],
        children: [
          { path: "borrowed-book", component: BorrowedBooksComponent, canActivate: [UserService] },
          { path: "history", component: HistoryComponent, canActivate: [UserService] },
          { path: "profile", component: ProfileComponent, canActivate: [UserService] },
        ],
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }