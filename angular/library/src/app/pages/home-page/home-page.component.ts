import { Component, OnInit } from "@angular/core";
import { BookService } from "src/app/services/book.service";
import { HomeService } from "src/app/services/home.service";
import { Router } from "@angular/router";

@Component({
  selector: "app-home-page",
  templateUrl: "./home-page.component.html",
  styleUrls: ["./home-page.component.scss"],
})
export class HomePageComponent implements OnInit {
  data: any;
  feature: any;
  currentUser: any;
  constructor(private homeService: HomeService, private router: Router) {}

  ngOnInit() {
    this.getHeader();
    this.getFeature();
    this.getCurrentUser();
  }

  getHeader() {
    console.log("allala");
    this.homeService.getHeader().subscribe((res) => {
      this.data = res;
      console.dir(this.data);
    });
  }
  getFeature() {
    console.log("allala");
    this.homeService.getFeature().subscribe((res: any) => {
      this.feature = res.data;
      console.dir(this.feature);
    });
  }
  getCurrentUser() {
    this.currentUser = JSON.parse(localStorage.getItem("currentUser"));
    return JSON.parse(localStorage.getItem("currentUser"));
  }
  checkCurrentUser() {
    this.getCurrentUser();
    if (this.currentUser) {
      if(this.currentUser.data.user.role == 'admin') {
        this.router.navigate(["/home/admin"]);
      } else if (this.currentUser.data.user.role == 'USER'){
        this.router.navigate(["/dashboard/browse-book"]);
      }
    } 
     else {
      this.router.navigate(["/books"]);
    }
  }
}