import { Component, OnInit } from "@angular/core";
import { BookService } from "src/app/services/book.service";
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: "app-books-page",
  templateUrl: "./books-page.component.html",
  styleUrls: ["./books-page.component.scss"],
})
export class BooksPageComponent implements OnInit {
  currentUser: any;
  constructor(private booksService: BookService, public route: ActivatedRoute) {}
  //
  ngOnInit() {
    this.getCurrentUser();
  }
  getCurrentUser() {
    this.currentUser = JSON.parse(localStorage.getItem("currentUser"));
    return JSON.parse(localStorage.getItem("currentUser"));
  }
}
