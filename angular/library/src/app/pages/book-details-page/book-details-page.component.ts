import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { BookService } from "src/app/services/book.service";
import { formatDate } from "@angular/common";

@Component({
  selector: "app-book-details-page",
  templateUrl: "./book-details-page.component.html",
  styleUrls: ["./book-details-page.component.scss"],
})
export class BookDetailsPageComponent implements OnInit {
  slug: string;
  currentUser: any;
  bookDetail: any;
  bookId: any;
  myDate = new Date();
  constructor(
    private router: Router,
    private bookService: BookService,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.getCurrentUser();
    this.slug = this.route.snapshot.paramMap.get("slug");
    console.log(this.slug)
    this.getBookDetail(this.slug);
  }

  getCurrentUser() {
    this.currentUser = JSON.parse(localStorage.getItem("currentUser"));
    return JSON.parse(localStorage.getItem("currentUser"));
  }
  getBookDetail(slug) {
    return this.bookService.getBookDetail(slug).subscribe((res: any) => {
      this.bookDetail = res.data[0];
      this.bookId = this.bookDetail.id
      console.log(this.bookId)
    });
  }
  borrow() {
    console.log(this.bookId)
    const cValue = formatDate(new Date(Date.now()), "yyyy-MM-dd", "en-US");
    const returnDate = formatDate(
      new Date(Date.now() + 12096e5),
      "yyyy-MM-dd",
      "en-US"
    );
    let body = {
      id_user: this.currentUser.data.user.id,
      id_book: this.bookId,
      date_borrowed: cValue,
      date_return: returnDate,
    };
    this.bookService.borrowBook(body).subscribe((res) => {
      this.updateBookFalse(body.id_book);
    });
  }
  updateBookFalse(id) {
    let body = {
      id: id,
    };
    this.bookService.updateBookFalse(body).subscribe((res) => {
      console.dir(res);
      this.getBookDetail(this.slug);
    });
  }
}
