import { Component, OnInit, Input } from "@angular/core";
import { BookService } from "src/app/services/book.service";
import { UserService } from "src/app/services/user.service";

@Component({
  selector: "app-borrowed-books",
  templateUrl: "./borrowed-books.component.html",
  styleUrls: ["./borrowed-books.component.scss"],
})
export class BorrowedBooksComponent implements OnInit {
  @Input() currentUser;
  borrowBooks: any;
  currentUserId: any;
  myCurrentsBooks: [] = [];
  constructor(
    private bookService: BookService,
    private userService: UserService
  ) {}

  ngOnInit() {
    this.getCurrentUser();
    this.currentUserId = this.currentUser.data.user.id;
    this.getBorrowBooks();
  }
  getCurrentUser() {
    this.currentUser = JSON.parse(localStorage.getItem("currentUser"));
    return JSON.parse(localStorage.getItem("currentUser"));
  }
  getBorrowBooks() {
    const bookToReturn: any[] = [];

    this.userService
      .getBorrowBooks(this.currentUserId)
      .subscribe((res: any) => {
        this.borrowBooks = res.message;
        console.dir(this.borrowBooks);
        this.borrowBooks.forEach((item: any) => {
          console.dir(item.returned);
          item.returned === 0 ? bookToReturn.push(item) : console.log("non");
        });

        this.borrowBooks = bookToReturn;
      });
    console.log(bookToReturn);
  }
  returnBook(bookId) {
    let body = {
      id: bookId,
    };
    this.bookService.returnBook(body).subscribe((res) => {
      this.getBorrowBooks();
      this.updateBookTrue(bookId);
    });
  }
  updateBookTrue(id) {
    let body = {
      id: id,
    };
    this.bookService.updateBookTrue(body).subscribe((res) => {
      console.dir(res);
    });
  }
}
