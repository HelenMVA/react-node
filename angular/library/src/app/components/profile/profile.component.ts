import { Component, OnInit, Input } from "@angular/core";

@Component({
  selector: "app-profile",
  templateUrl: "./profile.component.html",
  styleUrls: ["./profile.component.scss"],
})
export class ProfileComponent implements OnInit {
  @Input() currentUser;
  constructor() {}

  ngOnInit() {
    this.getCurrentUser();
  }

  getCurrentUser() {
    this.currentUser = JSON.parse(localStorage.getItem("currentUser"));
    return JSON.parse(localStorage.getItem("currentUser"));
  }
}
