import { Component, OnInit, Input } from "@angular/core";
import { BookService } from "src/app/services/book.service";
import { UserService } from "src/app/services/user.service";

@Component({
  selector: "app-history",
  templateUrl: "./history.component.html",
  styleUrls: ["./history.component.scss"],
})
export class HistoryComponent implements OnInit {
  currentUser;
  borrowBooks: any;
  currentUserId: any;
  booksReturned: any[] = [];
  constructor(private userService: UserService) {}

  ngOnInit() {
    this.getCurrentUser();
    this.currentUserId = this.currentUser.data.user.id;
    this.getBorrowBooks();
  }
  getCurrentUser() {
    this.currentUser = JSON.parse(localStorage.getItem("currentUser"));
    return JSON.parse(localStorage.getItem("currentUser"));
  }
  getBorrowBooks() {
    this.userService
      .getBorrowBooks(this.currentUserId)
      .subscribe((res: any) => {
        this.borrowBooks = res.message;
        console.dir(this.borrowBooks);
        this.borrowBooks.forEach((item: any) => {
          console.dir(item.returned);
          item.returned === 1
            ? this.booksReturned.push(item)
            : console.log("non");
        });
      });
  }
}
