import { Component, OnInit, Output, EventEmitter } from "@angular/core";
import { ModalRegisterComponent } from "../modal-register/modal-register.component";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { ModalLoginComponent } from "../modal-login/modal-login.component";
import { UserService } from "src/app/services/user.service";
import { Router } from "@angular/router";

@Component({
  selector: "app-nav",
  templateUrl: "./nav.component.html",
  styleUrls: ["./nav.component.scss"],
})
export class NavComponent implements OnInit {
  @Output() user: EventEmitter<any> = new EventEmitter();
  currentUser: any;
  constructor(
    private modalService: NgbModal,
    private userService: UserService,
    private router: Router
  ) {}

  ngOnInit() {
    this.getCurrentUser();
    console.log(this.currentUser)
  }

  getCurrentUser() {
    this.currentUser = JSON.parse(localStorage.getItem("currentUser"));
    this.user = this.currentUser;
    return JSON.parse(localStorage.getItem("currentUser"));
  }

  showInscriptionModal() {
    this.modalService.open(ModalRegisterComponent);
  }

  showConnectionModal() {
    const modalRef = this.modalService.open(ModalLoginComponent);
    modalRef.result.then((result) => {
      this.currentUser = result;
      // this.addNewItem(this.currentUser);
    });
  }
  // addNewItem(value) {
  //   console.dir(value);
  //   this.user.emit(value);
  //   console.dir(this.user);
  // }

  logout() {
    this.userService.logOut();
    this.getCurrentUser();
  }
  goUserSpace(currentuser) {
    const userId = currentuser.data.user.id;
    this.router.navigate(["user/" + userId], { state: { currentuser } });
  }
}
