import { Component, OnInit, Input } from "@angular/core";
import { BookService } from "src/app/services/book.service";
import { Router } from "@angular/router";
import { FilterServices } from "src/app/services/filter.service";
import { AuthorServices } from "src/app/services/author.service";

@Component({
  selector: "app-books-guest",
  templateUrl: "./books-guest.component.html",
  styleUrls: ["./books-guest.component.scss"],
})
export class BooksGuestComponent implements OnInit {
  allBooks: any;
  currentUser: any;
  allCategories: any;
  allAuthors: any;
  bookByCategory: any;
  selectedLevel;
  filetrBook: any[] = [];
  ctgId: any = "Categories";
  authorId: any = "Authors";
  constructor(
    private booksService: BookService,
    private router: Router,
    private bookSevice: BookService,
    private filterService: FilterServices,
    private authorService: AuthorServices
  ) {}

  ngOnInit() {
    this.getBooks();
    this.getCurrentUser();
    this.getAllCategories();
    this.getAllAuthors();
  }
  getBooks() {
    this.booksService.getAllBooks().subscribe((res: any) => {
      this.allBooks = res.data;
      this.filetrBook = res.data;
    });
  }
  goByBookId(id) {
    this.router.navigate(["books/" + id], { state: { id } });
  }

  getCurrentUser() {
    this.currentUser = JSON.parse(localStorage.getItem("currentUser"));
    return JSON.parse(localStorage.getItem("currentUser"));
  }
  getAllCategories() {
    this.bookSevice.getAllCategories().subscribe((res: any) => {
      this.allCategories = res.data;
    });
  }

  getAllAuthors() {
    this.authorService.getAuthors().subscribe((res: any) => {
      this.allAuthors = res.data;
    });
  }
  filterAuthor(event) {
    this.authorId = event.target.value;
    console.dir(this.authorId);
    if (this.ctgId !== "Categories") {
      console.dir("category  choisi ");
      if (this.authorId == "Authors" ) {
        console.dir("choisir all authors");
        this.filetrBook = this.allBooks.filter(
          (book) => book.category_id == this.ctgId
        );
      } else {
        console.dir("choisir un author");
        this.filetrBook = this.allBooks.filter(
          (book) =>
            book.category_id == this.ctgId && book.auter_id == this.authorId
        );
      }
    } else {
      console.dir("category nest pas choisi");
      if (this.authorId == "Authors") {
        console.dir("all authors");
        this.filetrBook = this.allBooks;
      } else {
        console.dir("author choisi");
        this.filetrBook = this.allBooks.filter(
          (book) => book.auter_id == this.authorId
        );
      }
    }
  }
  filterCategory(event) {
    this.ctgId = event.target.value;
    console.dir(this.authorId);
    console.log(this.ctgId )
    if (this.authorId !== "Authors" && this.authorId !== "Author") {
      console.dir("Author est choisy");
      console.dir(this.authorId);

      if (this.ctgId == "Categories") {
        console.dir("choisir all Categories!!!!!!");
        console.dir(this.authorId);
        this.filetrBook = this.allBooks.filter(
          (book) => book.auter_id == this.authorId
        );
      } else {
        console.dir("choisir un category");
        this.filetrBook = this.allBooks.filter(
          (book) =>
            book.category_id == this.ctgId && book.auter_id == this.authorId
        );
      }
    } else {
      console.dir("Author nest pas choisy");
      if (this.ctgId == "Categories") {
        console.dir("all categories");
        this.filetrBook = this.allBooks;
      } else {
        console.dir("category choisi");
        this.filetrBook = this.allBooks.filter(
          (book) => book.category_id == this.ctgId
        );
      }
    }
  }
}