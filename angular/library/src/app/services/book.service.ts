import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: "root",
})
export class BookService {
  apiURL = "http://localhost:8000/api/book/";
  constructor(private http: HttpClient) {}

  getAllBooks() {
    return this.http.get(`${this.apiURL}`);
  }
  getBookDetail(slug) {
    return this.http.get(`${this.apiURL}${slug}`);
  }
  borrowBook(body) {
    return this.http.post(`${this.apiURL}borrow`, body);
  }
  updateBookFalse(id) {
    return this.http.put(`${this.apiURL}available`, id);
  }
  updateBookTrue(id) {
    return this.http.put(`${this.apiURL}available/true`, id);
  }

  returnBook(body) {
    return this.http.put(`${this.apiURL}return`, body);
  }
  getAllCategories() {
    return this.http.get(`${this.apiURL}categories`);
  }
}
