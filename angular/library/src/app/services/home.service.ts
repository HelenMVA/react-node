import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: "root",
})
export class HomeService {
  apiURL = "http://localhost:8000/api/home";


  constructor(private http: HttpClient) {}

  getHeader() {
    return this.http.get(`${this.apiURL}`);
  }
  getFeature() {
    return this.http.get(`${this.apiURL}/features`);
  }
}
