import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Router } from '@angular/router';

@Injectable({
  providedIn: "root",
})
export class AdminService {
  apiURL = "http://localhost:8000/api/admin/";
  constructor(private http: HttpClient, private router: Router) {}

  getAllBooks() {
    return this.http.get(`${this.apiURL}`);
  }

  registerBook(data) {
    return this.http.post(`${this.apiURL}registerBook`, data);
  }
  deleteBook(id) {
    //  return this.http.delete(`${this.apiURL}${id}`);
    console.log("deletbook ser" + id);
    return this.http.delete(`http://localhost:8000/api/admin/book/${id}`);
  }
  getAllCategories() {
    return this.http.get(`http://localhost:8000/api/book/categories`);
  }
  // getAllCategory() {
  //   return this.http.get(`${this.apiURL}categories`);
  // }
  getCurrentUser() {
    return JSON.parse(localStorage.getItem('currentUser'));
  }

  canActivate() {
    const currentUser = this.getCurrentUser();

    return currentUser.data.user.role == 'admin' ? true : this.router.navigate(['/home']);
  }

  getUsers() {
    return this.http.get(`http://localhost:8000/api/user`);
}
}