import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { catchError, mapTo, tap } from "rxjs/operators";
import { Router } from "@angular/router";

@Injectable({
  providedIn: "root",
})
export class UserService {
  apiURL = "http://localhost:8000/api/user/";
  constructor(private http: HttpClient, private router: Router) { }
  register(data) {
    return this.http.post(`${this.apiURL}register`, data);
  }

  connect(connectUser) {
    console.dir(connectUser);
    return this.http.post(`${this.apiURL}authenticate`, connectUser).pipe(
      tap((user) => this.doLogin(user)),
      mapTo(true),
      catchError((err) => err)
    );
  }

  doLogin(user: any) {
    localStorage.setItem("currentUser", JSON.stringify(user));
    this.router.navigate(["/userDashboard"]);
  }
  logOut() {
    localStorage.removeItem("currentUser");
    this.router.navigate(['/home'])
  }

  getBorrowBooks(id) {
    return this.http.get(`${this.apiURL}${id}`);
  }

  getCurrentUser() {
    return JSON.parse(localStorage.getItem('currentUser'));
  }

  canActivate() {
    const currentUser = this.getCurrentUser();

    return currentUser ? true : this.router.navigate(['/home']);
  }
}
