import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: "root",
})
export class FilterServices {
  apiURL = "http://localhost:8000/api/filter/";
  constructor(private http: HttpClient) {}

  filterCategories(id) {
    console.log(id)
    return this.http.get(`${this.apiURL}category/${id}`);
  }

  filterAuthor(id) {
    console.log(id)
    return this.http.get(`${this.apiURL}author/${id}`);
  }
}
