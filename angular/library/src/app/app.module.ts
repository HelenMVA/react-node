import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { HomePageComponent } from "./pages/home-page/home-page.component";
import { BooksPageComponent } from "./pages/books-page/books-page.component";
import { BookDetailsPageComponent } from "./pages/book-details-page/book-details-page.component";
import { AdminDachboardPageComponent } from "./pages/admin-dachboard-page/admin-dachboard-page.component";
import { NavComponent } from "./components/nav/nav.component";
import { HttpClient, HttpClientModule } from "@angular/common/http";
import { ModalLoginComponent } from "./components/modal-login/modal-login.component";
import { ModalRegisterComponent } from "./components/modal-register/modal-register.component";
import { NgbActiveModal, NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { ReactiveFormsModule } from "@angular/forms";
import { BooksGuestComponent } from "./components/books-guest/books-guest.component";
import { BorrowedBooksComponent } from "./components/borrowed-books/borrowed-books.component";
import { HistoryComponent } from "./components/history/history.component";
import { ProfileComponent } from "./components/profile/profile.component";
import { DashboardComponent } from "./pages/dashboard/dashboard.component";
import { HomeDashboardComponent } from "./components/home-dashboard/home-dashboard.component";
import { BookListComponent } from './admin/book-list/book-list.component';
import { ModelAddBookComponent } from './admin/model-add-book/model-add-book.component';
import { GestionComponent } from './admin/gestion/gestion.component';

@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    BooksPageComponent,
    BookDetailsPageComponent,
    AdminDachboardPageComponent,
    NavComponent,
    ModalLoginComponent,
    ModalRegisterComponent,
    BooksGuestComponent,
    BorrowedBooksComponent,
    HistoryComponent,
    ProfileComponent,
    DashboardComponent,
    HomeDashboardComponent,
    BookListComponent,
    ModelAddBookComponent,
    GestionComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    NgbModule,
    ReactiveFormsModule,
  ],
  providers: [HttpClient, NgbActiveModal],
  bootstrap: [AppComponent],
  entryComponents: [ModalLoginComponent, ModalRegisterComponent],
})
export class AppModule {}
