# Project "Jean librairie"

<!-- :point_right: You can see the last Figma Wireframe [here](https://www.figma.com/file/HjvZ8ZlbjnD50xRK7QlXKo/quizz?node-id=0%3A1). -->

:mag_right: **Application description**

This projetct is about a book store where you can borrow few physical books as a customer for a limited time (15 days) and find information about author with bio.

## PREREQUISITES

> Git.
> MySql on port:3306.
> npm.

## QUICK-INSTALL

### :zap: INSTALL

1. Clone this repository.

2. In Application/config, copy database.dist.json to database.json, fill with the correct values.

3. If it's the first time that you're running this project, go to the _Application_ folder, at the root of the repository, with your terminal and run the following command:
   `npm install`

### :cyclone: LAUNCH

1. To start backend: Go to the _Application_ folder, at the root of the repository, with a separate terminal and run the following command:
   `npm run dev-back`

   You can now access to your Api at 'http://localhost:8000'

2. To start frontend: Go to the _Application_ folder, at the root of the repository, with a separate terminal and run the following command:
   `ng serve`

   You can now access to your front at 'http://localhost:4200'

_No needs to create the database on your mysql server, if it does not exists, it'll be created while runing the app_

## FOLLOW OUR WORK

<!-- If you have access to it (ask us an access by sending us your email) you can see our work in progress on our [Trello](https://trello.com/b/k87B2FK9/quizz). -->

---

## :scroll: ENTITIES

| users    | user_has_book | books     | author   | category     | home             | functionality |
| -------- | ------------- | --------- | -------- | ------------ | ---------------- | ------------- |
| ID       | ID            | ID        | ID       | ID           | ID               | ID            |
| USERNAME | ID_USER       | TITLE     | ID       | ID           | TITLE            | TITLE         |
| EMAIL    | ID_LIVRE      | AUTHER_ID | USER_ID  | ANSWER(TEXT) | SLOGAN           | DESCRIPTION   |
| PASSWORD | DATE_BORROWED | RESUMER   | SCORE    | GAME_ID      | LOGO             |               |
| ROLE     | DATE_RETURN   | DATE_PUB  | DURATION | QUESTION_ID  | IMG              |               |
|          |               | DISPO     | RATIO    | ANSWER_ID    | ID_functionality |               |
|          |

---

## :star2: TEAM

:email: kula.jega@gmail.com

:email: aluq93@gmail.com

:email: helen_moiseeva@yahoo.com
